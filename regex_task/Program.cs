﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
namespace regex_task
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the sentence to count the number of emails!");

            string input = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine("Enter the sentence to count the number of emails");
                input = Console.ReadLine();
            }

            MatchCollection emailMatch = findEmails(input);
            int count = emailMatch.Count;

            if (count > 0)
            {
                Console.WriteLine($"A total of {count}, email(s) was found.");
                foreach (var email in emailMatch)
                {
                    Console.WriteLine(email);
                }
            }
            else
            {
                Console.WriteLine("No email was found.");
            }

            Console.ReadLine();
        }

        static MatchCollection findEmails(string input)
        {
            Regex regex = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);

            MatchCollection emailMatch = regex.Matches(input);
            return emailMatch;
        }
    }
}
